<?php

namespace App\Controller;


use App\Entity\post\Tag;
use App\Form\SearchType;
use App\Model\SearchData;
use App\Repository\PostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/etiquettes')]
class TagController extends AbstractController{

    #[Route('/{slug}', name: 'tag.index',methods: ["GET"])]
    public function index (PaginatorInterface $paginatorInterface ,PostRepository $postRepository ,
                           Tag $tag , Request $request):Response{
        $searchData = new SearchData();
        $form= $this->createForm(SearchType::class,$searchData);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $searchData->page = $request->query->getInt('page',1);
            $posts = $postRepository->findBySearch($searchData);
            return $this->render('pages/blog/index.html.twig',[
                'tag' => $tag,
                'form' =>$form->createView(),
                'posts' => $posts,

            ]);
        }
        $data= $postRepository->findPublished();
        $posts = $paginatorInterface->paginate(
            $data,
            $request->query->getInt('page',1),
            9

        );
        return $this->render('pages/category/index.html.twig',[
            'tag' => $tag,
            'form' => $form->createView(),
            "posts"=>$posts
        ]);
    }
}