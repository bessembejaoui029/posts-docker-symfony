<?php
namespace App\Controller;

use App\Entity\post\Comment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class CommentController extends AbstractController{
    #[Route('/comment/{id}', name: 'comment.delete')]
    #[Security("is_granted('ROLE_USER') and user === comment.getAuthor()")]
    public function delete(Comment $comment, EntityManagerInterface $manager, Request $request){
        $params= ['slug' =>$comment->getPost()->getSlug()];
        if($this->isCsrfTokenValid('delete' . $comment->getId(), $request->request->get('_token'))){
            $manager->remove($comment);
            $manager->flush();
        }
        $this->addFlash('success', 'Votre commentaire a bien été supprimé.');

        return $this->redirectToRoute('post.show',[$params]);
    }
}