<?php

namespace App\Controller;

use App\Entity\post\Post;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class LikeController extends AbstractController{
    #[Route ('/like/post/{id}',name: "like.post")]
    #[IsGranted('ROLE_USER')]
    public function like(Post $post,EntityManagerInterface $manager):Response
    {
        $user = $this->getUser();
        if($post->isLikedByUser($user)){
            $post->removeLike($user);
            $manager->flush();
            
            return $this->json([
                'message' =>'like a ete supprimer',
                'nbLike' => $post->howManyLikes()

            ]);
        }
        $post->addLike($user);
        $manager->flush();

        return $this->json([
            'message' =>'like a ete ajouter',
            'nbLike' => $post->howManyLikes()
        ]);

    }
}